# Proyecto Django - ABM

## Pasos

* Crear el proyecto en una carpeta con el comando "python manage.py startapp nombreapp"
* Dentro de la carpeta del proyecto, crear una nueva carpeta con el nombre "templates"
* Editar archivo "settings.py"
```
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        (*) 'DIRS': ['templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
```
* Abrir terminal y ejecutar comando "python manage.py startapp appprincipal"
* En settings.py, agregar la "appprincipal"
```
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    (*) 'appprincipal',
]
```
* Editar "urls.py", incluir "path('', include('appprincipal.urls'))" 
```
from django.contrib import admin
from django.urls import path
from django.urls.conf import include
urlpatterns = [
    path('admin/', admin.site.urls),
    (*) path('', include('appprincipal.urls')),
]
```
* En "appprincipal", crear el archivo "urls.py"
* En "views.py", crear las funciones
* Editar "urls.py" de la appprincipal
```
from django.contrib import admin
from django.urls import path
(*) from django.urls.conf import include
(*) from .views import index
urlpatterns = [
    (*) path('', index, name='index')
]
```
* Ejecutar "python manage.py runserver"
* Editar "views.py" de "appprincipal"
* En la carpeta "templates", crear los archivos HTML
* En appprincipal, agregar los modelos
* Ejecutar "python manage.py makemigrations" y "python manage.py migrate"
* Se continúa agregando rutas en "appprincipal/urls.py"