from django.contrib import admin
from django.urls import path
from django.urls.conf import include
from .views import *

urlpatterns = [
    path('', index, name='index'),

    path('usuarios', usuarios, name='usuarios'),
    path('guardar_usuario', guardar_usuario, name='guardar_usuario'),
    path('editar_usuario/<int:myid>/', editar_usuario, name='editar_usuario'),
    path('actualizar_usuario/<int:myid>/', actualizar_usuario, name='actualizar_usuario'),
    path('eliminar_usuario/<int:myid>/', eliminar_usuario, name='eliminar_usuario'),

    path('tareas', tareas, name='tareas'),
    path('guardar_tarea', guardar_tarea, name='guardar_tarea'),
    path('editar_tarea/<int:myid>/', editar_tarea, name='editar_tarea'),
    path('actualizar_tarea/<int:myid>/', actualizar_tarea, name='actualizar_tarea'),
    path('terminar_tarea/<int:myid>/', terminar_tarea, name='terminar_tarea'),
    path('eliminar_tarea/<int:myid>/', eliminar_tarea, name='eliminar_tarea'),

]
