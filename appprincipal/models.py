from django.db import models

# Create your models here.

class Usuario(models.Model):
    apellidos = models.CharField(max_length=100, null=True)
    nombres = models.CharField(max_length=100, null=True)
    correo_electronico = models.EmailField(max_length = 200, null=True)

class Tarea(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=250)
    fechaLimite = models.DateField()
    terminada = models.BooleanField()

    usuario_id = models.OneToOneField(
        Usuario,
        on_delete=models.CASCADE, 
        null=True
    )

